function show(rowId, teamLink, teamName){
	closeOldTeam(rowId);
	document.getElementById(rowId).style.display = "block";
	document.getElementById(rowId).innerHTML += '<div class="left"><div id="lastGame"></div>'
												+ '<div id="performance"></div></div>'
												+ '<div class="right"><div id="allGames"></div>'
												+ '<div id="headToHead"></div></div>';
	
	//console.log(teamLink);
	showLastGame(teamLink);
	showPerformance(teamLink, teamName);
	showAllMatches(teamLink);
}

function closeOldTeam(rowId){
	for(var i = 0; i < 20; i++){
		if(i != rowId){
			document.getElementById(i).innerHTML = "";
			//console.log(document.getElementById(i).style.display);
		}
	}
};
/*----------------------------display last match result------------------*/
function showLastGame(teamLink){
	var tableHead = '<table width="100%" text-align="center">'
					+'<tr>'
						+'<td id="matchTime" colspan="5"></td>'
					+ '</tr>'
					+ '<tr>'
						+ '<td colspan="2" height="15xp"></td>'
						+ '<td id="matchResult" rowspan="2" align="center" width="100px"></td>'
						+ '<td colspan="2" height="15xp"></td>'
					+ '</tr>'
					+ '<tr>'
							+ '<td id="homeTeamName" align="right"></td>'
							+ '<td id="homeTeamPic" align="right"></td>'
							+ '<td id="awayTeamPic" align="left"></td>'
							+ '<td id="awayTeamName" align="left"></td>'
						+ '</tr>'
						+ '<tr>'
							+ '<td id="halfMatch" colspan="5" align="center"></td>'
						+ '</tr>'
					+ '</table>';
					
	document.getElementById("lastGame").innerHTML = '<h4>Last Match</h4>' + tableHead;
		
	$.ajax({
		headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
		url: teamLink + "/fixtures",
		dataType: 'json',
		type: 'GET',
	}).done(function(res){
		//console.log(last.fixtures);
		var fixture = res.fixtures;
		for(var i = res.count - 1; i >= 0; i--){
			
			if(fixture[i].status == "FINISHED"){
				
				var date = 'Time: '+fixture[i].date.substring(0,10)
						+ '<br><span style="margin-left:15px;color:black;background:rgba(240,240,240,0.5)">'
						+fixture[i].date.substring(11,19)+'</span>';
				
				document.getElementById("matchTime").innerHTML = date;
				
				var result = fixture[i].result.goalsHomeTeam + ' - '
						+ fixture[i].result.goalsAwayTeam;
				
				document.getElementById("matchResult").innerHTML = result;
				
				if(fixture[i].result.halfTime != null){
					var halfTime = 'Half Time:' + fixture[i].result.halfTime.goalsHomeTeam
									+ ' - ' + fixture[i].result.halfTime.goalsAwayTeam;
					
					document.getElementById("halfMatch").innerHTML = halfTime;
				}
				
				var homeTeamLink = 'https' + fixture[i]._links.homeTeam.href.substring(4);
				getLogo(homeTeamLink,"homeTeamPic","homeTeamName");
				
				var awayTeamLink = 'https' + fixture[i]._links.awayTeam.href.substring(4);
				getLogo(awayTeamLink,"awayTeamPic","awayTeamName");
				
				i=-1;
			}			
		}
	});
}

function getLogo(urlLink,id,name){
	//console.log(urlLink);
	$.ajax({
		headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
		url: urlLink,
		dataType: 'json',
		type: 'GET',
	}).done(function(res) {
		var crestUrl = res.crestUrl.substring(0,5) == "https"? res.crestUrl: "https"+res.crestUrl.substring(4);
		var img = '<img src="'+crestUrl+'" width=35px height=35px>';
		var code = res.code;
		document.getElementById(id).innerHTML = img;
		document.getElementById(name).innerHTML = code;
	});
}

/*-------------------------display last 5 matches performance----------------*/
function showPerformance(teamLink, teamName){
	$.ajax({
		headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
		url: teamLink + "/fixtures",
		dataType: 'json',
		type: 'GET',
	}).done(function(res){
		//console.log(res.fixtures);
		var fixture = res.fixtures;
		
		var win = 0, loss = 0, draw = 0, gf = 0, ga = 0, score = 0, streak = 0;		
		var j = 0;
		
		for(var i = res.count-1;i>=0;i--){
			if(fixture[i].status == "FINISHED"){
				if(teamName == fixture[i].homeTeamName){
					if(fixture[i].result.goalsHomeTeam > fixture[i].result.goalsAwayTeam){
						win++;
						score += 1.75;
						gf += fixture[i].result.goalsHomeTeam;
						ga += fixture[i].result.goalsAwayTeam;
						if(streak > 0){
							streak++;
						}
						else{
							streak=1;
						}
					}
					else if(fixture[i].result.goalsHomeTeam < fixture[i].result.goalsAwayTeam){
						loss++;
						gf += fixture[i].result.goalsHomeTeam;
						ga += fixture[i].result.goalsAwayTeam;
						if(streak < 0){
							streak--;
						}
						else{
							streak = -1;
						}
					}
					else{
						draw++;
						score += 3.5;
						gf += fixture[i].result.goalsHomeTeam;
						ga += fixture[i].result.goalsAwayTeam;
					}
				}
				else{
					if(fixture[i].result.goalsHomeTeam < fixture[i].result.goalsAwayTeam){
						win++;
						score += 4.8;
						ga += fixture[i].result.goalsHomeTeam;
						gf += fixture[i].result.goalsAwayTeam;
						if(streak > 0){
							streak++;
						}
						else{
							streak=1;
						}
					}
					else if(fixture[i].result.goalsHomeTeam > fixture[i].result.goalsAwayTeam){
						loss++;
						ga += fixture[i].result.goalsHomeTeam;
						gf += fixture[i].result.goalsAwayTeam;
						if(streak < 0){
							streak--;
						}
						else{
							streak = -1;
						}
					}
					else{
						draw++;
						score += 3.5;
						ga += fixture[i].result.goalsHomeTeam;
						gf += fixture[i].result.goalsAwayTeam;
					}
				}
				j++;
				if(j == 5)
				{
					i = -1;
				}
			}
		}
		var total = '<h4>Last 5 Matches Performance</h4>'
					+ 'W-L-D: ' + win + '-' + loss + '-' + draw
					+ '&#160; &#160; &#160; &#160; &#160; &#160;'
					+ 'Goals For: ' + gf
					+ '&#160; &#160; &#160; &#160; &#160; &#160;'
					+ 'Goals Against: ' + ga
					+ '<br>'
					+ 'Points: ' + Math.round(score)
					+ '&#160; &#160; &#160; &#160; &#160; &#160;'
					+ 'Win percentage: ' + (win/5*100) + '%'
					+ '&#160; &#160; &#160; &#160; &#160; &#160;'
					+ 'Streak: ' + streak;
		document.getElementById("performance").innerHTML += total + '<br><br>';
	});
}

/*-----------------------------show all the matches result-----------------*/
function showAllMatches(teamLink){
	$.ajax({
		headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
		url: teamLink + "/fixtures",
		dataType: 'json',
		type: 'GET',
	}).done(function(res){
		//console.log(res.fixtures);
		var fixture = res.fixtures;
		
		var allGames = "";
		for(var i = res.count-1; i >= 0; i--){
			//console.log(fix[i]);
			var fixtureLink = "'"+teamLink+"/fixtures'";
			var homeTeamName = "'"+fixture[i].homeTeamName+"'";
			var awayTeamName = "'"+fixture[i].awayTeamName+"'";
			var id = "'"+"hToh"+i+"'";
			allGames += '<tr>'
						+'<td colspan="3" style="font-size:12px;padding-top:15px;text-align:center">'
							+ '<span style="padding:3px 3px 1px 3px;color:rgb(255,100,20)">'
								+fixture[i].date.substring(0,10)
							+'</span>'
						+'</td>'
					+ '</tr>'
					+ '<tr id="all" class="hide" onmouseover="headToHead('+fixtureLink+','+homeTeamName+','+awayTeamName+','+id+')" onmouseout="noDisplayHoH()">'
						+ '<td align="right" style="background:rgba(5,5,5,0.5);color:rgba(250,250,250,0.5);font-weight:900;font-size:12px;height:40px">'
							+fixture[i].homeTeamName
						+'</td>'
						+ '<td align="center" width="80px" style="padding:10px;background:rgba(5,5,5,0.5);font-weight:600;height:60px;font-size:13px;">';
						if(fixture[i].status == "TIMED" || fixture[i].status == "SCHEDULED"){
							allGames += 'Start Time<br>'+fixture[i].date.substring(11,19); 
						} 
						else if(fixture[i].status == "FINISHED"){
							allGames += fixture[i].result.goalsHomeTeam 
										+ ' - '
										+ fixture[i].result.goalsAwayTeam;
						} else {
							allGames += "0 - 0<br>Postponed";
						}
					allGames +='</td>'
						+ '<td align="left" style="background:rgba(5,5,5,0.5);color:rgba(250,250,250,0.5);font-weight:900;font-size:12px;height:40px">'
							+fixture[i].awayTeamName
						+'</td>'
			+ '</tr>';
		}
		document.getElementById("allGames").innerHTML = '<table id="aR" align="center" width="100%">'
														+ '<captiom><h4>All Matches<h4></caption>'
														+allGames+ '</table>';
	});
}

/*-----------------------------display head to head--------------------*/
function headToHead(fixtureLink,homeTeamName,awayTeamName,id){
	$.ajax({
	headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
	url: fixtureLink,
	dataType: 'json',
	type: 'GET',
	}).done(function(res) {
		//console.log(rival);
		var fixture = res.fixtures;
		var h2h = "";
		for(var i = res.count-1; i >= 0; i--){
			if(fixture[i].status == "FINISHED"){
				var homeName = fixture[i].homeTeamName;
				var awayName = fixture[i].awayTeamName;
						
				if((homeTeamName == homeName && awayTeamName == awayName)||(homeTeamName == awayName && awayTeamName == homeName)){
					h2h += '<tr>'
							+'<td colspan="3" style="padding:10px 5px 5px 5px;font-size:10px;">'
								+ fixture[i].date.substring(0,10)
							+'</td>'
						+ '</tr>'
						+ '<tr id="all">'
							+ '<td align="center" style="border-right:3px solid blue;font-weight:600">'
							+ homeName
							+'</td>'
							+ '<td align="center" width="50px" style="padding:3px;font-size:14px;">'
							+ fixture[i].result.goalsHomeTeam + ' - '
							+ fixture[i].result.goalsAwayTeam
							+'</td>'
							+ '<td align="center" style="border-left:3px solid red;font-weight:600">'
							+ awayName
							+'</td>'
						+ '</tr>';			
				}
			}
		}
		document.getElementById("headToHead").innerHTML = '<div class="back"><table id="h2h" align="center" width="100%">'
													+'<caption>Head to Head</caption>'
													+h2h
													+'</table><div>';
	});
	document.getElementById("headToHead").style.display ="block";
}

function noDisplayHoH(){
	document.getElementById("headToHead").style.display ="none";
}