var time = new Date();
var year = time.getFullYear()-1;
var leagueId;

/*-------------------- get football league id ----------------*/
var getLeagueId = $.ajax({
 headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
 url: 'https://api.football-data.org/v1/competitions/?season='+year,
 dataType: 'json',
 type: 'GET',
}).done(function(res) {
	//console.log(res);
	res.forEach(league => {
		if(league.league === "PL"){
			leagueId = league.id;
		}
	});
});

/*---------- get league caption (league header)--------- */
getLeagueId.then(function(){ 
	$.ajax({
		headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
		url: 'https://api.football-data.org/v1/competitions/'+leagueId+'/leagueTable/',
		dataType: 'json',
		type: 'GET',
	}).done(function(res) {
		document.getElementById("head").innerHTML="<h2>" + res.leagueCaption + "</h2>";
		//console.log(res.leagueCaption);
	});
});
/*---------------get Table league---------------*/
function getRankingTable(){
	/*****get table title********/
	getLeagueId.then(function(){
		var title = "";
		var pageWidth = parseInt(window.innerWidth);
	
		if(pageWidth < 500){
			title += '<div>Pos.</div><div></div><div style="width:16.66%;">Club</div>'+ 
					"<div>Pl</div><div>W</div>"+
					"<div>D</div><div>L</div>"+
					"<div>GF</div><div>GA</div><div>GD</div>"+
					"<div>Pts</div>";
		}
		else{
			title += '<div>Pos.</div><div></div><div style="width:16.66%;">Club</div>'+ 
					"<div>Played</div><div>Wins</div>"+
					"<div>Draws</div><div>Losses</div>"+
					"<div>GF</div><div>GA</div><div>GD</div>"+
					"<div>Pts</div>";
		}
		document.getElementById("title").innerHTML = title;
	
	/******************get table content********************/
		$.ajax({
			headers: { 'X-Auth-Token': 'a6d185568af249c09ac753acf3edd83e' },
			url: 'https://api.football-data.org/v1/competitions/'+leagueId+'/leagueTable/',
			dataType: 'json',
			type: 'GET',
		}).done(function(res) {
			var content = "";
			var teams = res.standing;
			//console.log(teams);
			for(var i = 0; i < teams.length; i++){
				
				var crestURI = "";
				
				if(teams[i].crestURI.substring(0,5) != "https"){
					crestURI = "https" + teams[i].crestURI.substring(4);
				} else {
					crestURI = teams[i].crestURI;
				}
				
				var teamLink = "'https" + teams[i]._links.team.href.substring(4) + "'";
				var rowId = "'" + i + "'";
				//console.log(teamLink);
				content += '<div><div class="row" onclick = "show('+ rowId +', '+ teamLink + ', ' + teams[i].name + ')">'						
						+ "<div>" + teams[i].position + '.</div>'
						+ '<div><img src="'+ crestURI + '" width="30px" height="30px"></div>';
						
				var teamName = "";
				
				if(pageWidth < 500){
					teamName = teams[i].teamName.substring(0,3).toUpperCase();
				}
				else{
					teamName = teams[i].teamName;
				}
				content += '<div class="teamName" style="width:16.66%;">' + teamName + '</div>'
						+ "<div>" + teams[i].playedGames + '</div>'
						+ "<div>" + teams[i].wins + '</div>'
						+ "<div>" + teams[i].draws + '</div>'
						+ "<div>" + teams[i].losses + '</div>'
						+ "<div>" + teams[i].goals + '</div>'
						+ "<div>" + teams[i].goalsAgainst + '</div>'
						+ "<div>" + teams[i].goalDifference + '</div>'
						+ "<div>" + teams[i].points + '</div>'
						+ "</div>"
						+ '<div id="'+i+'"></div></div>';
			}
			document.getElementById("content").innerHTML = content;
		});
	});
}
window.onload = getRankingTable();
window.onresize = getRankingTable;
